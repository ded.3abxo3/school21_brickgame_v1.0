#include <check.h>
#include <unistd.h>

#include "../inc/functions.h"

GameInfo_t *game_session = {0};
UserAction_t test_move = TM_NONE;
bool game_is_running = true;
WINDOW *board, *next, *nexttext, *score;
int maxscore = 0;

START_TEST(moves_test) {
  char keys[20] = "  a  d  ww  s  wdaaq";
  for (int i = 0; i < 20; i++) {
    if (game_session->points > maxscore) maxscore = game_session->points;
    game_is_running = game_tick(game_session, test_move);
    display_board(board, game_session);
    display_nexttext(nexttext);
    display_shape(next, game_session->next);
    display_score(score, maxscore, game_session);
    doupdate();
    mssleep(10);
    switch (keys[i]) {
      case 'a':
        test_move = TM_LEFT;
        break;
      case 'd':
        test_move = TM_RIGHT;
        break;
      case 'w':
        test_move = TM_CLOCK;
        break;
      case 's':
        test_move = TM_DROP;
        break;
      case 'q':
        game_is_running = false;
        test_move = TM_NONE;
        break;
      case ' ':
        test_move = TM_NONE;
      default:
        test_move = TM_NONE;
    }
    if (i < 19)
      ck_assert_int_eq(game_is_running, 1);
    else
      ck_assert_int_eq(game_is_running, 0);
  }
}
END_TEST

Suite *suite_tetris(void) {
  Suite *s = suite_create("suite_tetris");
  TCase *tc = tcase_create("case_tetris");

  tcase_add_test(tc, moves_test);

  suite_add_tcase(s, tc);
  return s;
}

int main() {
  /* Новая игра */
  game_session = game_create(GAME_ROWS, GAME_COLS);

  initscr();
  cbreak();
  noecho();
  keypad(stdscr, TRUE);
  timeout(0);
  curs_set(0);
  init_colors();

  board = newwin(game_session->rows + 2, 2 * game_session->cols + 2, 0, 0);
  nexttext = newwin(3, 12, 0, 2 * (game_session->cols + 1) + 1);
  next = newwin(4, 10, 2, 2 * (game_session->cols + 1) + 1);
  score =
      newwin(14, 50, game_session->rows - 12, 2 * (game_session->cols + 1) + 1);

  test_move = TM_NONE;
  maxscore = readmaxscore(0);

  int number_failed;
  Suite *s = suite_tetris();
  SRunner *sr = srunner_create(s);
  srunner_run_all(sr, CK_NORMAL);
  number_failed = srunner_ntests_failed(sr);
  srunner_free(sr);

  getchar();
  /* Деинициализация ncurses */
  wclear(stdscr);
  endwin();

  /*Сохранение max score */
  savemaxscore(readmaxscore(maxscore));
  /* Очистка памяти игры */
  game_delete(game_session);
  game_session = NULL;
  return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
  ;
}