#ifndef FUNCTIONS_H_
#define FUNCTIONS_H_

//#include "../../inc/defines.h"
#include "defines.h"

// Функции вывода
void display_board(WINDOW *w, GameInfo_t *obj);
void display_nexttext(WINDOW *w);
void display_shape(WINDOW *w, TShape block);
void display_score(WINDOW *w, int maxscore, GameInfo_t *tg);
void init_colors(void);

// Функции работы со структурой игры
void game_init(GameInfo_t *obj, int rows, int cols);
GameInfo_t *game_create(int rows, int cols);
void game_destroy(GameInfo_t *obj);
void game_delete(GameInfo_t *obj);

// Функции обработки фигур
char game_get_block(GameInfo_t *obj, int row, int column);
bool game_check(GameInfo_t *obj, int row, int col);
bool game_tick(GameInfo_t *obj, UserAction_t move);

// Прочие функции
void mssleep(int milliseconds);
int nanosleep(const struct timespec *req,
              struct timespec *rem);  // вставил определение из time.h чтобы
                                      // компилятор не ругался
int readmaxscore(int points);
void savemaxscore(int points);

#endif  // FUNCTIONS_H_