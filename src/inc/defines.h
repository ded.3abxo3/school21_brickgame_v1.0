#ifndef DEFINES_H_
#define DEFINES_H_

#include <ncurses.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

/* Размеры игрового поля */
#define GAME_ROWS 20
#define GAME_COLS 10

/* Кол-во ячеек в фигуре, количество ориентаций, количество фигур */
#define CELLS 4
#define SHAPE_ORIENTATIONS 4
#define SHAPE_QUANTITY 7

/* Уровни */
#define MAX_LEVEL 10
#define POINTS_PER_LVL 600

/* 2 символа пробела на одну ячейку фигуры, чтобы получался квадрат, а не
 * прямоугольник */
#define COLS_PER_CELL 2
/* Вывод символа пробела 2 раза подряд на 1 ячейку фигуры. Инвертируем и красим
 * в цвет соответствующей цветовой пары */
#define PUSH_CELL(w, x)                         \
  waddch((w), ' ' | A_REVERSE | COLOR_PAIR(x)); \
  waddch((w), ' ' | A_REVERSE | COLOR_PAIR(x))
/* 2 пустых пробела */
#define PUSH_EMPTY_CELL(w) \
  waddch((w), ' ');        \
  waddch((w), ' ')

#define MIN(X, Y) ((X) < (Y) ? (X) : (Y))

/* Блоки 1х1 в фигуре */
typedef enum {
  TC_EMPTY,
  TC_CELLI,
  TC_CELLJ,
  TC_CELLL,
  TC_CELLO,
  TC_CELLS,
  TC_CELLT,
  TC_CELLZ
} TCell;

#define TC_IS_EMPTY(x) ((x) == TC_EMPTY)
#define TC_IS_FILLED(x) (!TC_IS_EMPTY(x))

typedef enum {
  EMPTY_COLOR = 0,
  I_COLOR = 1,
  S_COLOR = 2,
  L_COLOR = 11,
  T_COLOR = 4,
  Z_COLOR = 5,
  J_COLOR = 3,
  O_COLOR = 213
} TColors;

typedef struct {
  int row;
  int col;
} TCoords;

/* Данные о фигуре (тип, ориентация и координаты) */
typedef struct {
  int type;
  int orientation;
  TCoords position;
} TShape;

/* Варианты клавиш управления */
typedef enum {
  TM_START,
  TM_PAUSE,
  TM_QUIT,
  TM_LEFT,
  TM_RIGHT,
  TM_CLOCK,
  TM_DROP,
  TM_NONE
} UserAction_t;

/* Данные по игре */
typedef struct {
  /* Поле: */
  int rows;
  int cols;
  char *board;
  /* Очки: */
  int points;
  int level;
  /* Фигуры на поле */
  TShape falling;
  TShape next;
  /* Количество тиков до изменения гравитации */
  int ticks_till_gravity;
  /* Макс.очков */
  int high_score;
} GameInfo_t;

/* Массив фигур (определен в файле functions.c) */
extern const TCoords SHAPES[SHAPE_QUANTITY][SHAPE_ORIENTATIONS][CELLS];

/* Массив уровней гравитации */
extern int GRAVITY_LEVEL[MAX_LEVEL + 1];

#endif  // DEFINES_H_