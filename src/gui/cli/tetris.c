#include "../../inc/functions.h"

int main() {
  GameInfo_t *game_session;
  UserAction_t move = TM_NONE;
  bool game_is_running = true;
  WINDOW *board, *next, *nexttext, *score;

  /* Новая игра */
  game_session = game_create(GAME_ROWS, GAME_COLS);

  /* Инициализация ncurses */
  initscr();  // инициализация ncurses
  cbreak();   // пропуск нажатий клавиш
  noecho();  // отключение отображения нажатой клавици
  keypad(stdscr, TRUE);  // подключаем клавиши управления (со стрелками)
  timeout(0);     // отключение блокировки на getch()
  curs_set(0);    // невидимый курсор
  init_colors();  // функция (display.c) инициализации цветовых пар для
                  // отображения блоков 1х2

  /* Создаем окна для отображения информации (поле, 2 окна для следующей фигуры
   * и окно результатов) */
  board = newwin(game_session->rows + 2, 2 * game_session->cols + 2, 0, 0);
  nexttext = newwin(3, 12, 0, 2 * (game_session->cols + 1) + 1);
  next = newwin(4, 10, 2, 2 * (game_session->cols + 1) + 1);
  score =
      newwin(14, 50, game_session->rows - 12, 2 * (game_session->cols + 1) + 1);

  move = TM_START;
  int maxscore = readmaxscore(0);

  /* ИГРА */
  while (game_is_running) {
    if (game_session->points > maxscore) maxscore = game_session->points;
    game_is_running = game_tick(
        game_session, move);  // получение данных от пользователя за 1 тик
    display_board(board, game_session);  // поле
    display_nexttext(nexttext);          // текст NEXT SHAPE
    display_shape(
        next,
        game_session->next);  // Отображение следующей фигуры в специальном окне
    display_score(score, maxscore, game_session);  // Отображение окна очков
    doupdate();
    mssleep(10);  // задержка

    switch (getch()) {
      case KEY_LEFT:
        move = TM_LEFT;
        break;
      case KEY_RIGHT:
        move = TM_RIGHT;
        break;
      case KEY_UP:
        move = TM_CLOCK;
        break;
      case KEY_DOWN:
        move = TM_DROP;
        break;
      case 'q':
        game_is_running = false;
        move = TM_NONE;
        break;
      case 'p':
        wclear(board);
        box(board, 0, 0);
        wmove(board, game_session->rows / 2,
              (game_session->cols * COLS_PER_CELL - 10) / 2);
        wprintw(board, "GAME PAUSED");
        wrefresh(board);
        timeout(-1);
        getch();
        timeout(0);
        move = TM_NONE;
        break;
      default:
        if (move != TM_START) move = TM_NONE;
    }

    /* СТАРТ ИГРЫ */
    if (move == TM_START) {
      display_nexttext(nexttext);                  // текст NEXT SHAPE
      display_shape(next, game_session->falling);  // Отображение следующей
                                                   // фигуры в специальном окне
      display_score(score, maxscore, game_session);  // Отображение окна очков
      wclear(board);
      box(board, 0, 0);
      wmove(board, game_session->rows / 2, 4);
      wprintw(board, "PRESS ANY KEY");
      wmove(board, game_session->rows / 2 + 1, 7);
      wprintw(board, "TO START");
      wrefresh(board);
      timeout(-1);
      getch();
      timeout(0);
      move = TM_NONE;
    }
  }

  /* Деинициализация ncurses */
  wclear(stdscr);
  endwin();

  /* Выход из игры */
  printf("Game over!\n");
  printf("Points: %d | Level: %d.\n", game_session->points,
         game_session->level);
  /*Сохранение max score */
  maxscore = readmaxscore(maxscore);
  savemaxscore(maxscore);
  printf("MAX SCORE %d SAVED\n", maxscore);
  /* Очистка памяти игры */
  game_delete(game_session);
  return 0;
}