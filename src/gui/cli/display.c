#include "../../inc/functions.h"

/* Вывод поля */
void display_board(WINDOW *w, GameInfo_t *game) {
  int i, j;
  box(w, 0, 0);
  for (i = 0; i < game->rows; i++) {
    wmove(w, 1 + i, 1);
    for (j = 0; j < game->cols; j++) {
      if (TC_IS_FILLED(game_get_block(game, i, j))) {
        PUSH_CELL(w, game_get_block(game, i, j));
      } else {
        PUSH_EMPTY_CELL(w);
      }
    }
  }
  wnoutrefresh(w);
}

/* Вывод очков */
void display_nexttext(WINDOW *w) {
  wclear(w);
  mvwprintw(w, 1, 1, "NEXT SHAPE");
  wnoutrefresh(w);
}

/* Вывод фигуры */
void display_shape(WINDOW *w, TShape block) {
  int b;
  TCoords c;
  wclear(w);
  box(w, 0, 0);
  if (block.type == -1) {
    wnoutrefresh(w);
    return;
  }
  for (b = 0; b < CELLS; b++) {
    c = SHAPES[block.type][block.orientation][b];
    wmove(w, c.row + 1, c.col * COLS_PER_CELL + 1);
    PUSH_CELL(w, block.type + 1);
  }
  wnoutrefresh(w);
}

/* Вывод очков */
void display_score(WINDOW *w, int maxscore, GameInfo_t *tg) {
  wclear(w);
  box(w, 0, 0);
  mvwprintw(w, 1, 1, "MAX SCORE: %d", maxscore);
  mvwprintw(w, 3, 1, "Score: %d", tg->points);
  mvwprintw(w, 4, 1, "Level: %d", tg->level);
  mvwprintw(w, 5, 1, "Gravity: %d", GRAVITY_LEVEL[tg->level]);
  mvwprintw(w, 7, 1, "CONTROLS:");
  mvwprintw(w, 8, 1, "LEFT/RIGHT - move");
  mvwprintw(w, 9, 1, "UP/DOWN - rotate/drop");
  mvwprintw(w, 10, 1, "p/q - pause/quit game");
  mvwprintw(w, 12, 1, "TETRIS BrickGame v1.0 [by willumye@School21]");
  wnoutrefresh(w);
}

/* Цветовые пары для блоков */
void init_colors(void) {
  start_color();
  init_pair(TC_CELLI, I_COLOR, COLOR_BLACK);  // красный
  init_pair(TC_CELLJ, J_COLOR, COLOR_BLACK);  // оранжевый
  init_pair(TC_CELLL, L_COLOR, COLOR_BLACK);  // желтый
  init_pair(TC_CELLO, O_COLOR, COLOR_BLACK);  // розовый
  init_pair(TC_CELLS, S_COLOR, COLOR_BLACK);  // зеленый
  init_pair(TC_CELLT, T_COLOR, COLOR_BLACK);  // синий
  init_pair(TC_CELLZ, Z_COLOR, COLOR_BLACK);  // фиолетовый
}