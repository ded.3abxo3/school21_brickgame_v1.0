#include <unistd.h>

#include "../../inc/functions.h"

int readmaxscore(int points) {
  char *savefile = "savegame.txt";
  FILE *fp = fopen(savefile, "rb");
  int maxscore = 0;
  if (fp != NULL) {
    fread(&maxscore, sizeof(maxscore), 1, fp);
    fclose(fp);
  } else {
    fprintf(stderr, "File [%s] not found...\n", savefile);
  }
  return maxscore >= points ? maxscore : points;
}

void savemaxscore(int points) {
  char *savefile = "savegame.txt";
  FILE *fp = fopen(savefile, "wb");
  if (fp)
    fwrite(&points, sizeof(points), 1, fp);
  else {
    printf("Can't open file\n");
    return;
  }
  fclose(fp);
}