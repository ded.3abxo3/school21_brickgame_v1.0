#include "../../inc/functions.h"

/* 3-хмерный массив фигур [номер фигуры][ориентация фигуры][массив с
 * координатами ячеек] */
const TCoords SHAPES[SHAPE_QUANTITY][SHAPE_ORIENTATIONS][CELLS] = {
    // I
    {{{1, 0}, {1, 1}, {1, 2}, {1, 3}},
     {{0, 2}, {1, 2}, {2, 2}, {3, 2}},
     {{3, 0}, {3, 1}, {3, 2}, {3, 3}},
     {{0, 1}, {1, 1}, {2, 1}, {3, 1}}},
    // J
    {{{0, 0}, {1, 0}, {1, 1}, {1, 2}},
     {{0, 1}, {0, 2}, {1, 1}, {2, 1}},
     {{1, 0}, {1, 1}, {1, 2}, {2, 2}},
     {{0, 1}, {1, 1}, {2, 0}, {2, 1}}},
    // L
    {{{0, 2}, {1, 0}, {1, 1}, {1, 2}},
     {{0, 1}, {1, 1}, {2, 1}, {2, 2}},
     {{1, 0}, {1, 1}, {1, 2}, {2, 0}},
     {{0, 0}, {0, 1}, {1, 1}, {2, 1}}},
    // O
    {{{0, 1}, {0, 2}, {1, 1}, {1, 2}},
     {{0, 1}, {0, 2}, {1, 1}, {1, 2}},
     {{0, 1}, {0, 2}, {1, 1}, {1, 2}},
     {{0, 1}, {0, 2}, {1, 1}, {1, 2}}},
    // S
    {{{0, 1}, {0, 2}, {1, 0}, {1, 1}},
     {{0, 1}, {1, 1}, {1, 2}, {2, 2}},
     {{1, 1}, {1, 2}, {2, 0}, {2, 1}},
     {{0, 0}, {1, 0}, {1, 1}, {2, 1}}},
    // T
    {{{0, 1}, {1, 0}, {1, 1}, {1, 2}},
     {{0, 1}, {1, 1}, {1, 2}, {2, 1}},
     {{1, 0}, {1, 1}, {1, 2}, {2, 1}},
     {{0, 1}, {1, 0}, {1, 1}, {2, 1}}},
    // Z
    {{{0, 0}, {0, 1}, {1, 1}, {1, 2}},
     {{0, 2}, {1, 1}, {1, 2}, {2, 1}},
     {{1, 0}, {1, 1}, {2, 1}, {2, 2}},
     {{0, 1}, {1, 0}, {1, 1}, {2, 0}}},
};

/* Количество тиков игры до смещения фигуры */
int GRAVITY_LEVEL[MAX_LEVEL + 1] = {56, 53, 50, 48, 46, 44, 42, 40, 38, 36, 32};

/* Считываем символ на поле по координатам */
char game_get_block(GameInfo_t *obj, int row, int column) {
  return obj->board[obj->cols * row + column];
}

/* Устанавливаем значение value по координатам на поле */
static void game_set_block(GameInfo_t *obj, int row, int column, char value) {
  obj->board[obj->cols * row + column] = value;
}

/* Проверка попадания состояния игры в поле */
bool game_check(GameInfo_t *obj, int row, int col) {
  return 0 <= row && row < obj->rows && 0 <= col && col < obj->cols;
}

/* Размещение состояния игры в поле */
static void game_put(GameInfo_t *obj, TShape block) {
  int i;
  for (i = 0; i < CELLS; i++) {
    TCoords cell = SHAPES[block.type][block.orientation][i];
    game_set_block(obj, block.position.row + cell.row,
                   block.position.col + cell.col, block.type + 1);
  }
}

/* Удаление состояния игры с поля (очистка) */
static void game_remove(GameInfo_t *obj, TShape block) {
  int i;
  for (i = 0; i < CELLS; i++) {
    TCoords cell = SHAPES[block.type][block.orientation][i];
    game_set_block(obj, block.position.row + cell.row,
                   block.position.col + cell.col, TC_EMPTY);
  }
}

/* Проверяем, может ли текущее состояние игры быть помещено в поле */
static bool game_fits(GameInfo_t *obj, TShape block) {
  int i, r, c;
  for (i = 0; i < CELLS; i++) {
    TCoords cell = SHAPES[block.type][block.orientation][i];
    r = block.position.row + cell.row;
    c = block.position.col + cell.col;
    if (!game_check(obj, r, c) || TC_IS_FILLED(game_get_block(obj, r, c))) {
      return false;
    }
  }
  return true;
}

/* Генерация случайной фигуры */
static int random_shape(void) { return rand() % SHAPE_QUANTITY; }

/* Устанавливаем next текущей фигурой и генерируем новый next */
static void game_new_falling(GameInfo_t *obj) {
  obj->falling = obj->next;
  obj->next.type = random_shape();
  obj->next.orientation = 0;
  obj->next.position.row = 0;
  obj->next.position.col = obj->cols / 2 - 2;
}

/*********************************************
                   ХОДЫ
**********************************************/

/* Прогоняем тик и смещаем фигуру на 1 строку вниз */
static void game_do_gravity_tick(GameInfo_t *obj) {
  obj->ticks_till_gravity--;
  if (obj->ticks_till_gravity <= 0) {
    game_remove(obj, obj->falling);
    obj->falling.position.row++;
    if (game_fits(obj, obj->falling)) {
      obj->ticks_till_gravity = GRAVITY_LEVEL[obj->level];
    } else {
      obj->falling.position.row--;
      game_put(obj, obj->falling);

      game_new_falling(obj);
    }
    game_put(obj, obj->falling);
  }
}

/* Движение влево и вправо */
static void game_move(GameInfo_t *obj, int direction) {
  game_remove(obj, obj->falling);
  obj->falling.position.col += direction;
  if (!game_fits(obj, obj->falling)) {
    obj->falling.position.col -= direction;
  }
  game_put(obj, obj->falling);
}

/* Роняем фигуру вниз */
static void game_down(GameInfo_t *obj) {
  game_remove(obj, obj->falling);
  while (game_fits(obj, obj->falling)) {
    obj->falling.position.row++;
  }
  obj->falling.position.row--;
  game_put(obj, obj->falling);
  game_new_falling(obj);
}

/* Вращаем фигуру */
static void game_rotate(GameInfo_t *obj, int direction) {
  game_remove(obj, obj->falling);

  while (true) {
    obj->falling.orientation =
        (obj->falling.orientation + direction) % SHAPE_ORIENTATIONS;
    if (game_fits(obj, obj->falling)) break;
    obj->falling.position.col--;
    if (game_fits(obj, obj->falling)) break;
    obj->falling.position.col += 2;
    if (game_fits(obj, obj->falling)) break;
    obj->falling.position.col--;
  }
  game_put(obj, obj->falling);
}

/* Действие в зависимости от хода */
static void game_handle_move(GameInfo_t *obj, UserAction_t move) {
  switch (move) {
    case TM_LEFT:
      game_move(obj, -1);
      break;
    case TM_RIGHT:
      game_move(obj, 1);
      break;
    case TM_DROP:
      game_down(obj);
      break;
    case TM_CLOCK:
      game_rotate(obj, 1);
      break;
    default:
      break;
  }
}

/* Заполнена ли линия */
static bool game_line_full(GameInfo_t *obj, int i) {
  int j;
  for (j = 0; j < obj->cols; j++) {
    if (TC_IS_EMPTY(game_get_block(obj, i, j))) return false;
  }
  return true;
}

/* Сдвиг верхних линий вниз на 1 */
static void game_shift_lines(GameInfo_t *obj, int r) {
  int i, j;
  for (i = r - 1; i >= 0; i--) {
    for (j = 0; j < obj->cols; j++) {
      game_set_block(obj, i + 1, j, game_get_block(obj, i, j));
      game_set_block(obj, i, j, TC_EMPTY);
    }
  }
}

/* Ищем заполненую строку, удаляем ее, сдвигаем и после цикла возвращаем
 * количество удаленных строк */
static int game_check_lines(GameInfo_t *obj) {
  int i, nlines = 0;
  game_remove(obj, obj->falling);
  for (i = obj->rows - 1; i >= 0; i--) {
    if (game_line_full(obj, i)) {
      game_shift_lines(obj, i);
      i++;
      nlines++;
    }
  }
  game_put(obj, obj->falling);
  return nlines;
}

/* Подсчет очков */
static void game_adjust_score(GameInfo_t *obj, int lines_cleared) {
  static int line_multiplier[] = {0, 100, 300, 700,
                                  1500};  // очков за 0, 1, 2, 3 и 4 линии
  obj->points += line_multiplier[lines_cleared];
  obj->level = MIN(MAX_LEVEL, (int)(obj->points / POINTS_PER_LVL));
}

/* Конец игры проверка */
static bool game_game_over(GameInfo_t *obj) {
  int i, j;
  bool over = false;
  game_remove(obj, obj->falling);
  for (i = 0; i < 2; i++) {
    for (j = 0; j < obj->cols; j++) {
      if (TC_IS_FILLED(game_get_block(obj, i, j))) {
        over = true;
      }
    }
  }
  game_put(obj, obj->falling);
  return over;
}

/************************************
      Функции игры
*************************************/

bool game_tick(GameInfo_t *obj, UserAction_t move) {
  int lines_cleared;
  game_do_gravity_tick(obj);
  game_handle_move(obj, move);
  lines_cleared = game_check_lines(obj);
  game_adjust_score(obj, lines_cleared);
  return !game_game_over(obj);
}

/* Инициализация структуры */
void game_init(GameInfo_t *obj, int rows, int cols) {
  obj->rows = rows;
  obj->cols = cols;
  obj->board = malloc(rows * cols);
  memset(obj->board, TC_EMPTY, rows * cols);
  obj->points = 0;
  obj->level = 0;
  obj->ticks_till_gravity = GRAVITY_LEVEL[obj->level];
  srand(time(NULL));
  game_new_falling(obj);
  game_new_falling(obj);
  obj->next.position.col = obj->cols / 2 - 2;
  // printf("%d", obj->falling.position.col);
}

GameInfo_t *game_create(int rows, int cols) {
  GameInfo_t *obj = malloc(sizeof(GameInfo_t));
  game_init(obj, rows, cols);
  return obj;
}

void game_destroy(GameInfo_t *obj) {
  free(obj->board);
  obj->board = NULL;
}

void game_delete(GameInfo_t *obj) {
  game_destroy(obj);
  free(obj);
  obj = NULL;
}

/* Пауза в миллисекундах */
void mssleep(int milliseconds) {
  struct timespec ts;
  ts.tv_sec = 0;
  ts.tv_nsec = milliseconds * 1000000;
  nanosleep(&ts, NULL);
}