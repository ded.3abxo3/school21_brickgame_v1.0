#!/bin/bash

# Запрашиваем у пользователя путь к каталогу
echo "Введите путь к каталогу дл установки игры:"
read dir_path

if [ -z "$dir_path"]; then 
    dir_path=$1
fi

# Проверяем, существует ли каталог
if [ -d "$dir_path" ]; then
    # Каталог уже существует, информируем пользователя
    echo "Каталог '$dir_path' уже существует."
else
    # Каталог не существует, создаем его
    mkdir -p "$dir_path"
    echo "Каталог '$dir_path' создан."
fi

source=$(echo $PWD)
# Переходим в каталог
cd "$dir_path" || { echo "Не удалось перейти в каталог '$dir_path'."; exit 1; }

# Проверяем, успешно ли мы перешли в каталог
if [ $? -eq 0 ]; then
    echo "Устанавливаем $3 в '$dir_path'."
    #cp $source/$1 $dir_path || { echo "Не удалось установить программу $1 в '$dir_path'."; exit 1; }
    #echo "Программа $1 успешно установлена в $dir_path"
    echo $dir_path > $source/$2
else
    echo "Произошла ошибка при попытке перейти в каталог '$dir_path'."
fi
