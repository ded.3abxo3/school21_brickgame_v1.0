#!/bin/bash

current_date=$(date +"%d-%m-%Y %H:%M")
git add .
git commit -m "$(echo $current_date)"
git push origin main
